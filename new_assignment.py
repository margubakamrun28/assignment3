#assignment1
#Take string from console. And find all unique characters.
#sample input: "unique"
#output: u, n, i, q, e

a = input(str())
a_array = []
for i in a:
    if i not in a_array:
        a_array.append(i)

print(a_array)

#Assignment 02

#Take two string from console. And find those characters who are common in both strings.

#Sample input 1: "google"
#Sample input 2: "facebook"
#output: o, e

sample1 = input(str())
sample2 = input(str())
commons = []

for i in sample1:
    if (i in sample2) and (i not in commons):
        commons.append(i)

print(commons)



#Assignment 03
#-------------
#Take two string from console. And find those characters who are not common in both strings.

#Sample input 1: "google"
#Sample input 2: "facebook"
#output: g, l, f, a, c, b, k

sample3 = input(str())
sample4 = input(str())

not_commons = []
for i in sample3:
    if (i not in sample4) and  (i not in not_commons):
        not_commons.append(i)
for item in sample4:
    if (item not in sample3) and (item not in not_commons):
        not_commons.append(item)

print(not_commons)









































