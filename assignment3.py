arr = [1, 2, 3, 10, 11, 16, 18, 4, 43, 2, 2, 4]

threshold1 = 2
threshold2 = 4

# assignment: 01 Check if threshold1 and threshold2 exist in arr
# output: True/False

threshold1_exists_in_arr = False


for element in arr:
    print("starting the loop")
    if element == threshold1:
        threshold1_exists_in_arr = True

print("list carries 2 :" , threshold1_exists_in_arr)

threshold2_exists_in_arr = False

for element in arr:
    print("starting the loop")
    if element == threshold2:
        threshold2_exists_in_arr = True

print("list carries 4 :" , threshold2_exists_in_arr)

# assignment: 02 how many times arr contains threshold1 and threshold2?
# output:
# 2 exists x times
# 4 exists y times
count_times_for_2 = 0
for element in arr:
    print("counting start")
    if element == threshold1:
        count_times_for_2 += 1
        continue
print("2 exists" , count_times_for_2 , "times")

count_times_for_4 = 0
for element in arr:
    print("counting start")
    if element == threshold2:
        count_times_for_4 += 1
        continue
print("4 exists" , count_times_for_4 , "times")

# ----------------------------------------------------
arr1 = [
    1, 2, 3,
    [
        "A", "B",
        [
            "string1", "string2", "string3"
        ]
    ]
]
# assignment: 03 print string3 from the arr1

print(arr1[3][2][2])

# assignment: 04 change string3 to "string4"

arr1[3][2][2] = "string4"
print(arr1[3][2][2])

#assignment: 05 remove "string4"

x = arr1[3][2].pop(2)
print(arr1)

#assignment: 06 add "string5" after string2

arr1[3][2].append("string5")
print(arr1)

# -----------------------------------------------
arr2 = [3, 5, 1, 3, 5, 4, 10]
# assignment: 07 sort the arr2
arr2.sort()
print(arr2)

#assignment: 08 reverse the arr2
arr2.reverse()
print(arr2)















































